package ru.t1.schetinin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.schetinin.tm.api.service.ICommandService;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.command.AbstractCommand;
import ru.t1.schetinin.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

}