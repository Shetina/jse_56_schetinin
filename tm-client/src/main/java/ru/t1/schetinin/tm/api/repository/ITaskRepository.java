package ru.t1.schetinin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name);

}