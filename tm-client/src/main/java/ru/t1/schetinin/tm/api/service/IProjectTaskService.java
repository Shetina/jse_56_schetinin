package ru.t1.schetinin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.dto.model.TaskDTO;

public interface IProjectTaskService {

    @NotNull
    TaskDTO bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    @Nullable
    TaskDTO unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}
