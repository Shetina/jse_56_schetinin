package ru.t1.schetinin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.repository.dto.IDTORepository;
import ru.t1.schetinin.tm.dto.model.AbstractModelDTO;
import ru.t1.schetinin.tm.enumerated.Sort;

import java.util.List;

public interface IDTOService<M extends AbstractModelDTO> extends IDTORepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

    void removeById(@Nullable String id) throws Exception;

}