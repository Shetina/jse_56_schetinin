package ru.t1.schetinin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.schetinin.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    @NotNull
    @Override
    public Class<SessionDTO> getEntity() {
        return SessionDTO.class;
    }

}
